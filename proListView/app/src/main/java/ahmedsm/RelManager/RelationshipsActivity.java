package ahmedsm.RelManager;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.util.Patterns;

/**
 *  Configure the main app activity
 */
public class RelationshipsActivity extends AppCompatActivity {

    private ListView mListView;
    private RelationshipsDbAdapter mDbAdapter;
    private RelationshipsSimpleCursorAdapter mCursorAdapter;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relationships); // set layour

        mListView = (ListView) findViewById(R.id.relationships_list_view);
        
        // Click an item on the list
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int masterListPosition, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(RelationshipsActivity.this);
                ListView modeListView = new ListView(RelationshipsActivity.this);
                String[] modes = new String[] { "View/Edit Relationship", "Delete Relationship" };
                ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(RelationshipsActivity.this,
                        android.R.layout.simple_list_item_1, android.R.id.text1, modes);
                modeListView.setAdapter(modeAdapter);
                builder.setView(modeListView);
                final Dialog dialog = builder.create();
                dialog.show();
                modeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (position == 0) {
                            int nId = getIdFromPosition(masterListPosition);
                            Relationship relationship = mDbAdapter.fetchRelationshipById(nId);
                            fireCustomDialog(relationship);
                          
                        } 
                        else {
                            mDbAdapter.deleteRelationshipById(getIdFromPosition(masterListPosition)); // Delete
                            mCursorAdapter.changeCursor(mDbAdapter.fetchAllRelationships());
                        }
                        dialog.dismiss();
                    }
                });
            }
        });

        mListView.setDivider(null);
        mDbAdapter = new RelationshipsDbAdapter(this);
        mDbAdapter.open();

        if (savedInstanceState == null) {
            //Clear all data
            mDbAdapter.deleteAllRelationships();
            //Add some data
            insertSomeRelationships(); // dummy data
        }

        // Initialize cursor
        Cursor cursor = mDbAdapter.fetchAllRelationships();
        String[] from = new String[]{RelationshipsDbAdapter.COL_NAME};
        int[] to = new int[]{R.id.row_text};

        // Initialize cursor adaptor
        mCursorAdapter = new RelationshipsSimpleCursorAdapter(RelationshipsActivity.this, R.layout.relationships_row, cursor, from, to, 0);

        mListView.setAdapter(mCursorAdapter);
        
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setLogo(R.mipmap.ic_group_white_24dp); // Header icon

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fireCustomDialog(null);
            }
        });


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
            mListView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
                @Override
                public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean
                        checked) {
                }

                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    MenuInflater inflater = mode.getMenuInflater();
                    inflater.inflate(R.menu.cam_menu, menu);
                    return true;
                }

                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false;
                }

                // Delete relationship
                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.menu_item_delete_relationship:
                            for (int nC = mCursorAdapter.getCount() - 1; nC >= 0; nC--) {
                                if (mListView.isItemChecked(nC)) {
                                    mDbAdapter.deleteRelationshipById(getIdFromPosition(nC));
                                }
                            }
                            mode.finish();
                            mCursorAdapter.changeCursor(mDbAdapter.fetchAllRelationships());
                            return true;
                    }
                    return false;
                }

                @Override
                public void onDestroyActionMode(ActionMode mode) {
                }
            });
        }
    }


    private int getIdFromPosition(int nC) {
        return (int)mCursorAdapter.getItemId(nC);
    }

    /**
     * Populate View/Edit Relationship box
     * @param relationship
     */
    private void fireCustomDialog(final Relationship relationship) {
        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_custom);
        TextView titleView = (TextView) dialog.findViewById(R.id.custom_title);
        final EditText editName = (EditText) dialog.findViewById(R.id.custom_edit_name);
        final EditText editDate = (EditText) dialog.findViewById(R.id.custom_edit_date);
        final EditText editPlans = (EditText) dialog.findViewById(R.id.custom_edit_plans);
        final EditText editPhone = (EditText) dialog.findViewById(R.id.custom_edit_phone);
        final EditText editEmail = (EditText) dialog.findViewById(R.id.custom_edit_email);
        Button saveButton = (Button) dialog.findViewById(R.id.custom_button_save);
        final CheckBox checkBox = (CheckBox) dialog.findViewById(R.id.custom_check_box);
        LinearLayout rootLayout = (LinearLayout) dialog.findViewById(R.id.custom_root_layout);
        final boolean isEditOperation = (relationship != null);

        // If editing a relationship, populate the fields with current data
        if (isEditOperation) {
            titleView.setText("View/Edit Relationship");
            checkBox.setChecked(relationship.getFavorite() == 1);
            editName.setText(relationship.getName());
            editDate.setText(relationship.getDate());
            editPlans.setText(relationship.getPlans());
            editPhone.setText(relationship.getPhone());
            editEmail.setText(relationship.getEmail());
            rootLayout.setBackgroundColor(getResources().getColor(R.color.green));
        }
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String relName = editName.getText().toString(); // pull out user input
                String relDate = editDate.getText().toString();
                String relPlans = editPlans.getText().toString();
                String relPhone = editPhone.getText().toString();
                String relEmail = editEmail.getText().toString();

                // Validate phone number
                if (!relPhone.equals("") && !isValidPhone(relPhone)) {
                    Toast.makeText(getApplicationContext(), "You have entered an invalid phone number for " + relName + ".", Toast.LENGTH_LONG).show();
                }
                    if (isEditOperation) {
                        Relationship relEdited = new Relationship(relationship.getId(), relName, checkBox.isChecked() ? 1 : 0, relDate, relPlans, relPhone, relEmail);
                        mDbAdapter.updateRelationship(relEdited);

                    } 
                    else {
                        mDbAdapter.createRelationship(relName, checkBox.isChecked(), relDate, relPlans, relPhone, relEmail);
                    }

                // refresh main activity and close dialog box
                mCursorAdapter.changeCursor(mDbAdapter.fetchAllRelationships());
                dialog.dismiss();
            }
        });

        //Cancel
        Button buttonCancel = (Button) dialog.findViewById(R.id.custom_button_cancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    /**
     * Check for valid phone number formatting
     * @param phone
     * @return
     */
    private boolean isValidPhone(String phone) {
        return Patterns.PHONE.matcher(phone).matches();
    }

    /**
     * Insert dummy data for testing
     */
    private void insertSomeRelationships() {
        mDbAdapter.createRelationship("Reena", false, "January 2017", "Make dinner plans", "444-444-4444", "reena@hotmail.com");
        mDbAdapter.createRelationship("Mel D", false, "December 2016", "Ask for a coffee date", "222-222-2222", "mel@yahoo.com");
        mDbAdapter.createRelationship("Mia", true, "April 2017", "Play fetch!","444-444-4444","i am a dog");
        mDbAdapter.createRelationship("Aunt Penny", false, "June 2016", "Give her a call","777-777-7777","penny@hotmail.com");
        mDbAdapter.createRelationship("Nick", false, "September 2016", "Ask to a concert","123-456-7890","nick@gmail.com");
        mDbAdapter.createRelationship("Joe", false, "October 2016", "Send an email","333-333-3333","joe@uchicago.edu");
        mDbAdapter.createRelationship("Grandpa", true, "June 2016", "Give him a call","555-555-5555","bob@gmail.com");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate menu
        getMenuInflater().inflate(R.menu.menu_relationships, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        // Action bar options
        switch (id) {
            case R.id.action_new:
                fireCustomDialog(null);
                return true;
            case R.id.action_exit:
                finish();
                return true;
            default:
                return false;
        }

    }
}
