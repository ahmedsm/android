package ahmedsm.RelManager;

/**
 *  Relationship class constructs a new relationship and provides accessor methods for data fields
 */
public class Relationship {
    private int mId;
    private String mName;
    private int mFavorite;
    private String mDate;
    private String mPlans;
    private String mPhone;
    private String mEmail;

    /**
     * Construct a new relationship object
     * @param id
     * @param name
     * @param favorite
     * @param date
     * @param plans
     * @param phone
     * @param email
     */
    public Relationship(int id, String name, int favorite, String date, String plans, String phone, String email) {
        mId = id;
        mFavorite = favorite;
        mName = name;
        mDate = date;
        mPlans = plans;
        mPhone = phone;
        mEmail = email;
    }

    /**
     * Return unique ID
     * @return
     */
    public int getId() {
        return mId;
    }

    /**
     * Set ID
     * @param id
     */
    public void setId(int id) {
        mId = id;
    }

    /**
     * Return favorite flag
     * @return
     */
    public int getFavorite() {
        return mFavorite;
    }

    /**
     * Set favorite flag
     * @param favorite
     */
    public void setFavorite(int favorite) {
        mFavorite = favorite;
    }

    /**
     * Return contact name
     * @return
     */
    public String getName() {
        return mName;
    }

    /**
     * Return last contact date
     * @return
     */
    public String getDate() {
        return mDate;
    }

    /**
     * Return plans
     * @return
     */
    public String getPlans() {
        return mPlans;
    }

    /**
     * Return contact's phone number
     * @return
     */
    public String getPhone() {
        return mPhone;
    }

    /**
     * Return contact's email address
     * @return
     */
    public String getEmail() {
        return mEmail;
    }

    /**
     * Set contact name
     * @param name
     */
    public void setName(String name) {
        mName = name;
    }

    /**
     * Set last contact date
     * @param date
     */
    public void setDate(String date) {
        mDate = date;
    }

    /**
     * Set plans
     * @param plans
     */
    public void setPlans (String plans) {
        mPlans = plans;
    }

    /**
     * Set contact phone number
     * @param phone
     */
    public void setPhone (String phone) {
        mPhone = phone;
    }

    /**
     * Set contact email address
     * @param email
     */
    public void setEmail (String email) {
        mPhone = email;
    }
    
}
