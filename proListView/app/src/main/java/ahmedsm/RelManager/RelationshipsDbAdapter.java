package ahmedsm.RelManager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 *  Relationship database configuration
 */
public class RelationshipsDbAdapter {

    // Column names
    public static final String COL_ID = "_id";
    public static final String COL_NAME = "name";
    public static final String COL_FAVORITE = "favorite";
    public static final String COL_DATE = "date";
    public static final String COL_PLANS = "plans";
    public static final String COL_PHONE= "phone";
    public static final String COL_EMAIL = "email";

    // Column indices
    public static final int INDEX_ID = 0;
    public static final int INDEX_NAME= INDEX_ID + 1;
    public static final int INDEX_FAVORITE = INDEX_ID + 2;
    public static final int INDEX_DATE = INDEX_ID + 3;
    public static final int INDEX_PLANS= INDEX_ID + 4;
    public static final int INDEX_PHONE = INDEX_ID + 5;
    public static final int INDEX_EMAIL= INDEX_ID + 6;

    private static final String TAG = "RelationshipsDbAdapter";
    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;
    private static final String DATABASE_NAME = "dba_rels";
    private static final String TABLE_NAME = "tbl_rels";
    private static final int DATABASE_VERSION = 4;
    private final Context mCtx;

    // Construct SQL statement that will create the database
    private static final String DATABASE_CREATE =
            "CREATE TABLE if not exists " + TABLE_NAME + " ( " +
                    COL_ID + " INTEGER PRIMARY KEY autoincrement, " +
                    COL_NAME + " TEXT, " +
                    COL_FAVORITE + " INTEGER, " +
                    COL_DATE + " TEXT, " +
                    COL_PLANS + " TEXT, " +
                    COL_PHONE + " TEXT, " +
                    COL_EMAIL + " TEXT );";

    public RelationshipsDbAdapter(Context ctx) {
        this.mCtx = ctx;
    }
    //open
    public void open() throws SQLException {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
    }

    public void close() {
        if (mDbHelper != null) {
            mDbHelper.close();
        }
    }

    /**
     * Create a new relationship and add it to the database
     * @param name
     * @param favorite
     * @param date
     * @param plans
     * @param phone
     * @param email
     */
    public void createRelationship(String name, boolean favorite, String date, String plans, String phone, String email) {
        ContentValues values = new ContentValues();
        values.put(COL_NAME, name);
        values.put(COL_FAVORITE, favorite ? 1 : 0);
        values.put(COL_DATE, date);
        values.put(COL_PLANS, plans);
        values.put(COL_PHONE, phone);
        values.put(COL_EMAIL, email);
        mDb.insert(TABLE_NAME, null, values);
    }
    // overloaded version
    public long createRelationship(Relationship relationship) {
        ContentValues values = new ContentValues();
        values.put(COL_NAME, relationship.getName());
        values.put(COL_FAVORITE, relationship.getFavorite());
        values.put(COL_DATE, relationship.getDate());
        values.put(COL_PLANS, relationship.getPlans());
        values.put(COL_PHONE, relationship.getPhone());
        values.put(COL_EMAIL, relationship.getEmail());
        return mDb.insert(TABLE_NAME, null, values);
    }

    /**
     * Return a relationship by ID
     * @param id
     * @return
     */
    public Relationship fetchRelationshipById(int id) {

        Cursor cursor = mDb.query(TABLE_NAME, new String[]{COL_ID, COL_NAME, COL_FAVORITE, COL_DATE, COL_PLANS, COL_PHONE, COL_EMAIL}, COL_ID + "=?", new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        return new Relationship(cursor.getInt(INDEX_ID), cursor.getString(INDEX_NAME), cursor.getInt(INDEX_FAVORITE), cursor.getString(INDEX_DATE), cursor.getString(INDEX_PLANS), cursor.getString(INDEX_PHONE), cursor.getString(INDEX_EMAIL));

    }

    /**
     * Loop and return all relationships in database
     * @return
     */
    public Cursor fetchAllRelationships() {
        Cursor mCursor = mDb.query(TABLE_NAME, new String[]{COL_ID, COL_NAME, COL_FAVORITE, COL_DATE, COL_PLANS, COL_PHONE, COL_EMAIL}, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    /**
     * Update an existing relationship
     * @param rel
     */
    public void updateRelationship(Relationship rel) {
        ContentValues values = new ContentValues();
        values.put(COL_NAME, rel.getName());
        values.put(COL_FAVORITE, rel.getFavorite());
        values.put(COL_DATE, rel.getDate());
        values.put(COL_PLANS, rel.getPlans());
        values.put(COL_PHONE, rel.getPhone());
        values.put(COL_EMAIL, rel.getEmail());
        mDb.update(TABLE_NAME, values,
                COL_ID + "=?", new String[]{String.valueOf(rel.getId())});
    }

    /**
     * Delete relationship with a given ID
     * @param nId
     */
    public void deleteRelationshipById(int nId) {
        mDb.delete(TABLE_NAME, COL_ID + "=?", new String[]{String.valueOf(nId)});
    }

    /**
     * Delete all relationships in the database
     */
    public void deleteAllRelationships() {
        mDb.delete(TABLE_NAME, null, null);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.w(TAG, DATABASE_CREATE);
            db.execSQL(DATABASE_CREATE);
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);
        }
    }
}