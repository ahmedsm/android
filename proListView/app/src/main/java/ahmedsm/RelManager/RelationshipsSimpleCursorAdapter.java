package ahmedsm.RelManager;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.view.ViewGroup;

/**
 *  Configure cursor adapter
 */
public class RelationshipsSimpleCursorAdapter extends SimpleCursorAdapter {
    public RelationshipsSimpleCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return super.newView(context, cursor, parent);
    }
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        super.bindView(view, context, cursor);
        ViewHolder holder = (ViewHolder) view.getTag();
        if (holder == null) {
            holder = new ViewHolder();
            holder.colFav = cursor.getColumnIndexOrThrow(RelationshipsDbAdapter.COL_FAVORITE); // flag favorite contacts
            holder.listTab = view.findViewById(R.id.row_tab);
            view.setTag(holder);
        }
        if (cursor.getInt(holder.colFav) > 0) {
            holder.listTab.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));
        }
        else {
            holder.listTab.setBackgroundColor(context.getResources().getColor(R.color.sage));
        }
    }
    static class ViewHolder {
        int colFav;
        View listTab;
    }
}