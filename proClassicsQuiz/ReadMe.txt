Shaoly Ahmed
MPCS 51031
Project 1
04/12/2017

The proClassicsQuiz application presents the user with three possible quiz options:
Latin: Select the correct Latin translation of the given English word
Greek: Select the correct Greek translation of the given English word
Mixed: Select the correct translation (either Latin or Greek) of the given English word.  Only one correct translation will be presented.

The user can use either the buttons or the toolbar options to navigate through the app.

After starting a quiz, the user can see their results upon pressing the "Quit" button.  
The user will then have the option of starting a new quiz or trying the same quiz again.

The code is based heavily on the proCapitalsQuiz project that Dr. Gerber presented during class.
