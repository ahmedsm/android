package edu.uchicago.gerber.procapitalquiz;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Question class contains methods for creating a new translation question
 * and accessing the Latin and Greek translations of a given English word
 */
public class Question implements Serializable {

    //this overrides the serializable id
    private static final long serialVersionUID = 6546546516546843135L;

    private String mEnglish;
    private String mLatin;
    private String mGreek;
    private Set<String> mWrongAnswers = new HashSet<String>();

    /**
     * Build a new question with an English, Latin, and Greek set of translations
     * @param english - English work
     * @param latin - Latin translation
     * @param greek - Greek translations
     */
    public Question(String english, String latin, String greek) {
        this.mEnglish = english;
        this.mLatin = latin;
        this.mGreek = greek;
    }

    /**
     * Return Greek translation
     * @return
     */
    public String getGreek() {
        return mGreek;
    }

    /**
     * Return Latin translation
     * @return
     */
    public String getLatin() {
        return mLatin;
    }

    /**
     * Return English word
     * @return
     */
    public String getEnglish() {
        return mEnglish;
    }

    /**
     * Return a list of incorrect answers
     * @return
     */
    public Set<String> getWrongAnswers() {
        return mWrongAnswers;
    }

    /**
     * Add a new wrong answer
     * @param wrongAnswer
     * @return
     */
    public boolean addWrongAnswer(String wrongAnswer){
        return mWrongAnswers.add(wrongAnswer);
    }

    /**
     * Return the question prompt
     * @return
     */
    public String getQuestionText(){
        return "Which is the translation for '" + mEnglish + "'?";
    }
}
