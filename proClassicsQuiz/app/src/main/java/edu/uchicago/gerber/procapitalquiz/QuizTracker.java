package edu.uchicago.gerber.procapitalquiz;


//QuizTracker follows the singleton pattern. Here's what you need to do to create a singleton
public class QuizTracker {
    //1. strictly encapsulate members by making them all private
    private int mCorrectAnswerNum = 0;
    private int mIncorrectAnswerNum = 0;
    private String mName;
    private int mQuestionNum;
    //2. provide a static member of the same type of the enclosing class
    private  static QuizTracker sQuizTracker;

    //3. override its default constructor (remember that by default, Java provides a no-arg contractor)
    // and make it private. No one outside this class can instantiate it now.
    private QuizTracker(){}

    //4. provide a static getInstance() method. In this case, we don't instantiate until getInstance() is called for the first time (also know as lazy initialization)
    //All subsequent calls to getInstance() will return a reference to the already-instantiated sQuizTracker object. With this technique we can guarantee that
    //there is only ever one instance of this object, no matter how many times we call getInstance(). Because it is static, sQuizTracker it will persist in memory space throughout
    // the entire lifecycle of the app and accessible to any first-order component (e.g. Activity).

    //5. Activity members are often flushed -- for example in the event of a bounce.  Notice that QuizTracker is nowhere a member of any Activity.
    public static QuizTracker getInstance(){
        if (sQuizTracker == null){
            sQuizTracker = new QuizTracker();
            return sQuizTracker;
        }
        else {
            return sQuizTracker;
        }
    }

    /**
     * Set the current number of incorrect answers given by the user
     * @param incorrectAnswerNum
     */
    public void setIncorrectAnswerNum(int incorrectAnswerNum) {
        mIncorrectAnswerNum = incorrectAnswerNum;
    }

    /**
     * Set the current number of correct answers given by the user
     * @param correctAnswerNum
     */
    public void setCorrectAnswerNum(int correctAnswerNum) {
        mCorrectAnswerNum = correctAnswerNum;
    }

    /**
     * Start a new round, reset user's name and set user's score to 0
     */
    public void reset(){
        setName("");
        setQuestionNum(1);
        setIncorrectAnswerNum(0);
        setCorrectAnswerNum(0);
    }

    /**
     * Reset score so user can play again
     */
    public void again(){

        setQuestionNum(1);
        setIncorrectAnswerNum(0);
        setCorrectAnswerNum(0);
    }

    /**
     * Return the question number for the current round
     * @return
     */
    public int getQuestionNum() {
        return mQuestionNum;
    }

    /**
     * Set the question number
     * @param questionNum
     */
    public void setQuestionNum(int questionNum) {
        mQuestionNum = questionNum;
    }

    /**
     * Return user's name
     * @return
     */
    public String getName(){
        return mName;
    }

    /**
     * Set the user's name
     * @param name
     */
    public void setName(String name) {
        this.mName = name;
    }

    /**
     * Increment the number of incorrect answers
     */
    public void answeredWrong(){
        mIncorrectAnswerNum++;
    }

    /**
     * Increment the number of correct answers
     */
    public void answeredRight(){
        mCorrectAnswerNum++;
    }

    /**
     * Increment the question number
     */
    public void incrementQuestionNumber(){
        mQuestionNum++;
    }

    /**
     * Return the number of correct answers
     * @return
     */
    public int getCorrectAnswerNum(){
        return mCorrectAnswerNum;
    }

    /**
     * Return the number of incorrect answers
     * @return
     */
    public int getIncorrectAnswerNum(){
        return mIncorrectAnswerNum;
    }

    /**
     * Return total number of answers
     * @return
     */
    public int getTotalAnswers(){
        return mCorrectAnswerNum + mIncorrectAnswerNum;
    }


}
