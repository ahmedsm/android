package edu.uchicago.gerber.procapitalquiz;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

/**
 * QuestionActivity configures a new question with a set of possible answers and determines whether
 * the user selected the correct answer.
 */
public class QuestionActivity extends ActionBarActivity {

    public static final String QUESTION = "edu.uchicago.cs.quiz.gerber.QUESTION";
    private static final String DELIMITER = "\\|";
    private static final int NUM_ANSWERS = 5;
    private static final int ENGLISH = 0;
    private static final int LATIN = 1;
    private static final int GREEK = 2;
    private Random mRandom;
    private Question mQuestion;
    private String[] mTranslations;
    private boolean mItemSelected = false;
    private TextView mQuestionNumberTextView;
    private RadioGroup mQuestionRadioGroup;
    private TextView mQuestionTextView;
    private Button mSubmitButton;
    private Button mQuitButton;
    private int nQuizType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getIntent().getExtras(); // pull in the quiz type (Latin/Greek/Mixed) from the intent
        nQuizType = b.getInt("quizType");
        setContentView(R.layout.activity_question);
        //generate a question
        mTranslations = getResources().getStringArray(R.array.classic_words);

        //get refs to inflated members
        mQuestionNumberTextView = (TextView) findViewById(R.id.questionNumber);
        mQuestionTextView = (TextView) findViewById(R.id.questionText);
        mSubmitButton = (Button) findViewById(R.id.submitButton);

        //init the random
        mRandom = new Random();

        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { // submit button behavior
                submit();
            }
        });


        //set quit button action
        mQuitButton = (Button) findViewById(R.id.quitButton);
        mQuitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               displayResult();
            }
        });

        mQuestionRadioGroup = (RadioGroup) findViewById(R.id.radioAnswers);
        //disallow submitting until an answer is selected
        mQuestionRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                mSubmitButton.setEnabled(true);
                mItemSelected = true;
            }
        });

        fireQuestion(savedInstanceState);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //pass the question into the bundle when I have a config change
        outState.putSerializable(QuestionActivity.QUESTION, mQuestion);
    }

    /**
     * Get a new question and display it to the user
     */
    private void fireQuestion(){
        mQuestion = getQuestion();
        populateUserInterface();
    }


    //overloaded to take savedInstanceState
    private void fireQuestion(Bundle savedInstanceState){

        if (savedInstanceState == null ){
            mQuestion = getQuestion();
        } else {
            mQuestion = (Question) savedInstanceState.getSerializable(QuestionActivity.QUESTION);
        }

        populateUserInterface();

    }


    /**
     * Determine whether the user selected the correct answer after clicking the Submit button
     */
       private void submit() {

        Button checkedButton = (Button) findViewById(mQuestionRadioGroup.getCheckedRadioButtonId());
        String guess = checkedButton.getText().toString();
        //see if they guessed right
        if (mQuestion.getLatin().equals(guess) & nQuizType == 0) { // Latin quiz
            QuizTracker.getInstance().answeredRight();
        }
        else if(mQuestion.getGreek().equals(guess) & nQuizType == 1){ // Greek quiz
            QuizTracker.getInstance().answeredRight();
        }
        else if((mQuestion.getGreek().equals(guess) || mQuestion.getLatin().equals(guess)) & nQuizType == 2){ // Mixed quiz
            QuizTracker.getInstance().answeredRight();
        }
        else {
            QuizTracker.getInstance().answeredWrong();
        }
        if (QuizTracker.getInstance().getTotalAnswers() < Integer.MAX_VALUE) {
            //increment the question number
            QuizTracker.getInstance().incrementQuestionNumber();

            fireQuestion();
        } else {

            displayResult();
        }

    }

    /**
     * Display question number, question text, and possible answers
     */
    private void populateUserInterface() {
        //take care of button first
        mSubmitButton.setEnabled(false);
        mItemSelected = false;

        //populate the QuestionNumber textview
        String questionNumberText = getResources().getString(R.string.questionNumberText);
        int number = QuizTracker.getInstance().getQuestionNum();
        mQuestionNumberTextView.setText(String.format(questionNumberText, number));

        //set question text
        mQuestionTextView.setText(mQuestion.getQuestionText());

        //will generate a number 0-4 inclusive
        int randomPosition = mRandom.nextInt(NUM_ANSWERS);
        int counter = 0;
        mQuestionRadioGroup.removeAllViews();
        //for each of the 5 wrong answers
        for (String wrongAnswer : mQuestion.getWrongAnswers()) {
            if (counter == randomPosition & nQuizType == 0) { // Latin quiz
                //insert the cor answer
                addRadioButton(mQuestionRadioGroup, mQuestion.getLatin()); // display Latin translation
            }
            else if(counter == randomPosition & nQuizType == 1) { // Greek quiz
                //insert the cor answer
                addRadioButton(mQuestionRadioGroup, mQuestion.getGreek()); // display Greek translation
            }
            else if(counter == randomPosition & nQuizType == 2) { // Mixed quiz
                //insert the cor answer
                double dRand = Math.random(); // Randomly select either a correct Greek or Latin term
                if (dRand <= 0.5) {
                    addRadioButton(mQuestionRadioGroup, mQuestion.getGreek());
                }
                else if (dRand > 0.5) {
                    addRadioButton(mQuestionRadioGroup, mQuestion.getLatin());
                }
            }
            else {
                addRadioButton(mQuestionRadioGroup, wrongAnswer); // display an incorrect answer
            }
            counter++;
        }
    }

    /**
     * Configure radio buttons
     * @param questionGroup
     * @param text
     */
    private void addRadioButton(RadioGroup questionGroup, String text) {
        RadioButton button = new RadioButton(this);
        button.setText(text);
        button.setTextColor(Color.WHITE);
        button.setButtonDrawable(android.R.drawable.btn_radio);
        questionGroup.addView(button);

    }

    /**
     * Generate a correct answer and 4 incorrect answers
     * @return
     */
    private Question getQuestion() {
        //generate corr answer
        String[] strAnswers = getRandomTranslation();
        mQuestion = new Question(strAnswers[ENGLISH], strAnswers[LATIN], strAnswers[GREEK]);

        //generates 5 wrong answers
        while (mQuestion.getWrongAnswers().size() < NUM_ANSWERS) {
            String[] strTranslations = getRandomTranslation();

            //if the one we picked is equal to the answer OR
            //if is not from the same region as the answer OR
            //if we already picked this one
            //while (strTranslations[LATIN].equals(strAnswers[LATIN]) || !strTranslations[GREEK].equals(strAnswers[GREEK]) || mQuestion.getWrongAnswers().contains(strTranslations[LATIN])) {
            if(nQuizType == 0) { // Latin quiz returns Latin answers
                while (strTranslations[LATIN].equals(strAnswers[LATIN]) || mQuestion.getWrongAnswers().contains(strTranslations[LATIN])) {
                    //then we need pick another one
                    strTranslations = getRandomTranslation();
                }

                mQuestion.addWrongAnswer(strTranslations[LATIN]);
            }
            else if (nQuizType == 1) { // Greek quiz returns Greek answers
                while (strTranslations[GREEK].equals(strAnswers[GREEK]) || mQuestion.getWrongAnswers().contains(strTranslations[GREEK])) {
                    //then we need pick another one
                    strTranslations = getRandomTranslation();
                }
                mQuestion.addWrongAnswer(strTranslations[GREEK]);
            }
            else if (nQuizType == 2) { // Mixed quiz returns a random mix of Latin and Greek answers
                while (strTranslations[GREEK].equals(strAnswers[GREEK]) || strTranslations[LATIN].equals(strAnswers[LATIN]) || mQuestion.getWrongAnswers().contains(strTranslations[GREEK]) || mQuestion.getWrongAnswers().contains(strTranslations[LATIN])) {
                    //then we need pick another one
                    strTranslations = getRandomTranslation();
                }
                double dRand = Math.random(); // Randomly select either a Latin or Greek answer
                if (dRand >= 0.5) {
                    mQuestion.addWrongAnswer(strTranslations[GREEK]);
                }
                else if (dRand < 0.5) {
                    mQuestion.addWrongAnswer(strTranslations[LATIN]);
                }
            }

        }
        return mQuestion;
    }

    /**
     * Return a random translation from the string array
     * @return
     */
    private String[] getRandomTranslation() {
        int index = mRandom.nextInt(mTranslations.length);
        return mTranslations[index].split(DELIMITER);
    }


    /**
     * Display user's results
     */
    private void displayResult(){

        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra("quizType",nQuizType); // pass quiz type to ResultActivity
        startActivity(intent);
        finish();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_question, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuQuit:

                displayResult();
                return true;
            case R.id.menuSubmit:
                if(mItemSelected){

                    submit();
                }
                else{
                    Toast toast = Toast.makeText(this, getResources().getText(R.string.pleaseSelectAnswer), Toast.LENGTH_SHORT);
                    toast.show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
