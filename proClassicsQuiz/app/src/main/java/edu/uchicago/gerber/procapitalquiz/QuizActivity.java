package edu.uchicago.gerber.procapitalquiz;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 *
 */
public class QuizActivity extends ActionBarActivity {


    private Button mExitButton, mStartLatin, mStartGreek, mStartMixed;
    private String mName;
    private EditText mNameEditText;
    private int nQuizType; // type 0 = Latin, type 1 = Greek, type 2 = Mixed
    
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        //define behaviors
        mNameEditText = (EditText) findViewById(R.id.editName);

        //exit button
        mExitButton = (Button) findViewById(R.id.exitButton);
        mExitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //Latin start button
        mStartLatin = (Button) findViewById(R.id.latinButton);
        mStartLatin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startMe(0); // pass in quiz type as a parameter
            }
        });

        //Greek start button
        mStartGreek = (Button) findViewById(R.id.greekButton);
        mStartGreek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startMe(1);
            }
        });

        //Mixed start button
        mStartMixed = (Button) findViewById(R.id.mixedButton);
        mStartMixed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startMe(2);
            }
        });


    }

    /**
     * Start a new quiz
     * @param nQuizType
     */
    private void startMe(int nQuizType) {
        mName = mNameEditText.getText().toString();
        QuizTracker.getInstance().setName(mName); // Set user name
        askQuestion(1, nQuizType);
    }

    /**
     * Start new QuizActivity
     * @param number
     * @param nQuizType
     */
    private void askQuestion(int number, int nQuizType) {
        QuizTracker.getInstance().setQuestionNum(number); // Set question number in tracker
        Intent intent = new Intent(QuizActivity.this, QuestionActivity.class);
        intent.putExtra("quizType",nQuizType); // pass in quiz type
        startActivity(intent);
    }


    //these are for the menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_quiz, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuExit:
                finish();
                return true;
            case R.id.menuStartLatin:
                startMe(0);
                return true;
            case R.id.menuStartGreek:
                startMe(1);
                return true;
            case R.id.menuStartMixed:
                startMe(2);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
