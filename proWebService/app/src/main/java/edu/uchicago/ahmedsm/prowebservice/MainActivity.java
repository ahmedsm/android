package edu.uchicago.ahmedsm.prowebservice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Driver for weather app
 */
public class MainActivity extends Activity {


    public static final String LAT = "LATITUDE";
    public static final String LON = "LONGITUDE";

    private String mKey;

    private TextView mDate1;
    private TextView mMin1;
    private TextView mMax1;
    private TextView mAvg1;
    private TextView mSummary1;
    private ImageView mImg1;

    private TextView mDate2;
    private TextView mMin2;
    private TextView mMax2;
    private TextView mAvg2;
    private TextView mSummary2;
    private ImageView mImg2;

    private TextView mDate3;
    private TextView mMin3;
    private TextView mMax3;
    private TextView mAvg3;
    private TextView mSummary3;
    private ImageView mImg3;

    private EditText strLat;
    private EditText strLong;

    /**
     * Read in JSON stream
     * @param URL
     * @return
     */
    public String readJSON(String URL) {

        StringBuilder stringBuilder = new StringBuilder();
        HttpGet httpGet = new HttpGet(URL);
        HttpClient httpClient = new DefaultHttpClient();

        try {
            HttpResponse response = httpClient.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int code = statusLine.getStatusCode();
            if (code == 200) {
                HttpEntity httpEntity = response.getEntity();
                InputStream sInputStream = httpEntity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(sInputStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                sInputStream.close();
            } 
            else {
                Log.e("JSON", "Error reading file");
            }
        } 
        catch (Exception e) {
            Log.e("readJSON", e.getLocalizedMessage());
        }
        
        return stringBuilder.toString();
    }

    /**
     * Parse and display weather forecast
     */
    private class ReadWeatherTask extends AsyncTask<String, Void, String> {
        
        protected String doInBackground(String... urls) {
            return readJSON(urls[0]);
        }

        protected void onPostExecute(String result) {
            try {

                JSONObject jsonObject = new JSONObject(result);

                // error handling
                if (!jsonObject.getString("error").equals("0")) {
                    String err = jsonObject.getString("error_message");
                    Toast.makeText(getBaseContext(), err, Toast.LENGTH_LONG).show();
                }
                else {
                JSONArray weatherItems = new JSONArray(jsonObject.getString("forecast"));
                JSONObject weatherItem1 = weatherItems.getJSONObject(0);

                // Forecast Day 1
                String date1 = weatherItem1.getString("date");
                String fmtDate1 = date1.substring(5, 7) + "/" + date1.substring(8, 10);
                mDate1.setText(fmtDate1);
                mMin1.setText(weatherItem1.getString("min_f"));
                mMax1.setText(weatherItem1.getString("max_f"));
                mAvg1.setText(weatherItem1.getString("avg_f") + " \u2109");
                mSummary1.setText(weatherItem1.getString("summary"));

                String img1 =  weatherItem1.getString("icon");
                img1 = img1.substring(0,img1.indexOf("."));
                int res1 = getResources().getIdentifier(img1, "drawable", getPackageName());
                if (res1 == 0) {
                    mImg1.setImageResource(R.drawable.na); // Display generic image if matching image can't be found
                }
                else {
                    mImg1.setImageResource(res1);
                }

                // Forecast Day 2
                JSONObject weatherItem2 = weatherItems.getJSONObject(1);
                String date2 = weatherItem2.getString("date");
                String fmtDate2 = date2.substring(5, 7) + "/" + date2.substring(8, 10);
                mDate2.setText(fmtDate2);
                mMin2.setText(weatherItem2.getString("min_f"));
                mMax2.setText(weatherItem2.getString("max_f"));
                mAvg2.setText(weatherItem2.getString("avg_f") + " \u2109");
                mSummary2.setText(weatherItem2.getString("summary"));

                String img2 =  weatherItem2.getString("icon");
                img2 = img2.substring(0,img2.indexOf("."));
                int res2 = getResources().getIdentifier(img2, "drawable", getPackageName());
                if (res2 == 0) {
                    mImg2.setImageResource(R.drawable.na);
                }
                else {
                    mImg2.setImageResource(res2);
                }

                // Forecast Day 3
                JSONObject weatherItem3 = weatherItems.getJSONObject(2);
                String date3 = weatherItem3.getString("date");
                String fmtDate3 = date3.substring(5, 7) + "/" + date3.substring(8, 10);
                mDate3.setText(fmtDate3);
                mMin3.setText(weatherItem3.getString("min_f"));
                mMax3.setText(weatherItem3.getString("max_f"));
                mAvg3.setText(weatherItem3.getString("avg_f") + " \u2109");
                mSummary3.setText(weatherItem3.getString("summary"));

                String img3 =  weatherItem3.getString("icon");
                img3 = img3.substring(0,img3.indexOf("."));
                int res3 = getResources().getIdentifier(img3, "drawable", getPackageName());
                if (res3 == 0) {
                    mImg3.setImageResource(R.drawable.na);
                }
                else {
                    mImg3.setImageResource(res3);
                }

                }
            }
            catch (Exception e) {
                Log.d("ReadWeatherTask", e.getLocalizedMessage());
            }
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        strLat = (EditText) findViewById(R.id.strLat);
        strLong = (EditText) findViewById(R.id.strLong);
        mKey = getKey("open_key");

        // Shared preferences
        if (savedInstanceState == null && (PrefMgr.getString(this, LAT) == null && PrefMgr.getString(this, LON) == null)) {

            strLat.setText("42");
            strLong.setText("88");
            PrefMgr.setString(this, LAT, "42"); // Default to Chicago
            PrefMgr.setString(this, LON, "88");
        }
        
        else {

            strLat.setText(PrefMgr.getString(this, LAT)); // Pull in last entered values
            strLong.setText(PrefMgr.getString(this, LON));
        }

        // Initialize fields
        mDate1 = (TextView) findViewById(R.id.date1);
        mMin1 = (TextView) findViewById(R.id.min_f1);
        mMax1 = (TextView) findViewById(R.id.max_f1);
        mAvg1 = (TextView) findViewById(R.id.avg_f1);
        mSummary1 = (TextView) findViewById(R.id.summary1);
        mImg1 = (ImageView) findViewById(R.id.img1);
        
        mDate2 = (TextView) findViewById(R.id.date2);
        mMin2 = (TextView) findViewById(R.id.min_f2);
        mMax2 = (TextView) findViewById(R.id.max_f2);
        mAvg2 = (TextView) findViewById(R.id.avg_f2);
        mSummary2 = (TextView) findViewById(R.id.summary2);
        mImg2 = (ImageView) findViewById(R.id.img2);

        mDate3 = (TextView) findViewById(R.id.date3);
        mMin3 = (TextView) findViewById(R.id.min_f3);
        mMax3 = (TextView) findViewById(R.id.max_f3);
        mAvg3 = (TextView) findViewById(R.id.avg_f3);
        mSummary3 = (TextView) findViewById(R.id.summary3);
        mImg3 = (ImageView) findViewById(R.id.img3);

    }

    /**
     * Run query when Get Forecast button is clicked
     * @param view
     */
    public void btnGetForecast(View view) {

        new ReadWeatherTask().execute("https://www.amdoren.com/api/weather.php?api_key=" + mKey +  "&lat=" + strLat.getEditableText().toString() + "&lon=" + strLong.getText().toString());

        // Save preferences
        PrefMgr.setString(this, LAT, strLat.getEditableText().toString());
        PrefMgr.setString(this, LON, strLong.getEditableText().toString());

    }


    /**
     * Get API key
     * @param keyName
     * @return
     */
    private String getKey(String keyName){
        AssetManager assetManager = this.getResources().getAssets();
        Properties properties = new Properties();
        try {
            InputStream sInputStream = assetManager.open("keys.properties");
            properties.load(sInputStream);

        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return  properties.getProperty(keyName);

    }


}