package edu.uchicago.ahmedsm.prowebservice;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Store user preferences
 */
public class PrefMgr {

    private static SharedPreferences sSharedPreferences;

    public static void setString(Context context, String type, String value ){
        sSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(type, value);
        editor.commit();

    }

    public static String getString(Context context, String type){
        sSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sSharedPreferences.getString(type, null);

    }


}